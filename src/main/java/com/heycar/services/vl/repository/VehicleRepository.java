package com.heycar.services.vl.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.heycar.services.vl.model.Vehicle;

public interface VehicleRepository extends MongoRepository<Vehicle, String> {

	@Query(value = "{dealerId:?0, code:{$in:?1}}")
	List<Vehicle> findByDealerIdAndCodes(String dealerId, Collection<String> codes);

}
