package com.heycar.services.vl.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.heycar.services.vl.model.Dealer;

public interface DealerRepository extends MongoRepository<Dealer, String> {

}
