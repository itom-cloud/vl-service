package com.heycar.services.vl.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Document(collection = "vehicle")
@TypeAlias(value = "Vehicle")
@CompoundIndexes(@CompoundIndex(name = "code_dealer_index", def = "{'code':1, 'dealerId':1}", unique = true))
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = { "code", "dealerId" })
public class Vehicle {

	@Id
	private String id;
	private String code;
	private String make;
	private String model;
	private Integer power;
	private Integer year;
	private String color;
	private Integer price;
	private String dealerId;
	private LocalDateTime updatedOn;
}
