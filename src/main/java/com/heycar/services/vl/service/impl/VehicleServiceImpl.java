package com.heycar.services.vl.service.impl;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.heycar.services.vl.model.Vehicle;
import com.heycar.services.vl.repository.VehicleRepository;
import com.heycar.services.vl.rest.request.SearchCriteria;
import com.heycar.services.vl.rest.request.VehicleRequest;
import com.heycar.services.vl.service.VehicleService;

@Service
public class VehicleServiceImpl implements VehicleService {

	private final VehicleRepository vehicleRepository;
	private final MongoTemplate mongoTemplate;

	public VehicleServiceImpl(final VehicleRepository vehicleRepository, final MongoTemplate mongoTemplate) {
		super();
		this.vehicleRepository = vehicleRepository;
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public void saveAll(final String dealerId, final List<VehicleRequest> vehicleRequests) {
		final Map<String, Vehicle> updatableVehiclesMap = new HashMap<String, Vehicle>();
		vehicleRequests.forEach(vrequest -> {
			Vehicle vehicle = new Vehicle();

			BeanUtils.copyProperties(vrequest, vehicle);
			vehicle.setDealerId(dealerId);
			try {
				vehicle.setUpdatedOn(LocalDateTime.now());
				vehicleRepository.save(vehicle);
			} catch (DuplicateKeyException e) {
				updatableVehiclesMap.put(vehicle.getCode(), vehicle);
			}
		});

		if (updatableVehiclesMap.isEmpty())
			return;

		final List<Vehicle> existingVehicles = this.vehicleRepository.findByDealerIdAndCodes(dealerId,
				updatableVehiclesMap.keySet());
		existingVehicles.forEach(eVehicle -> {
			Vehicle updatable = updatableVehiclesMap.get(eVehicle.getCode());
			updatable.setId(eVehicle.getId());
			vehicleRepository.save(updatable);
		});
	}

	@Override
	public Page<Vehicle> findAll(SearchCriteria searchRequest) {
		final Criteria criteria = new Criteria();
		final Pageable page = PageRequest.of(searchRequest.getPageNumber(), searchRequest.getPageSize());
		if (!StringUtils.isEmpty(searchRequest.getColor())) {
			criteria.and("color").regex(searchRequest.getColor(), "i");
		}
		if (!StringUtils.isEmpty(searchRequest.getMake())) {
			criteria.and("make").regex(searchRequest.getMake(), "i");
		}
		if (!StringUtils.isEmpty(searchRequest.getModel())) {
			criteria.and("model").regex(searchRequest.getModel(), "i");
		}
		if (!StringUtils.isEmpty(searchRequest.getYear())) {
			criteria.and("year").is(searchRequest.getYear());
		}

		final Query query = new Query(criteria);
		query.with(page);

		List<Vehicle> data = mongoTemplate.find(query, Vehicle.class);
		long total = mongoTemplate.count(query, Vehicle.class);
		return new PageImpl<Vehicle>(data, page, total);

	}

}
