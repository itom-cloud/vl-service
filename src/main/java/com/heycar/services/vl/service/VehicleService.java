package com.heycar.services.vl.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.heycar.services.vl.model.Vehicle;
import com.heycar.services.vl.rest.request.SearchCriteria;
import com.heycar.services.vl.rest.request.VehicleRequest;

public interface VehicleService {

	public void saveAll(String dealerId, List<VehicleRequest> vehicles);

	public Page<Vehicle> findAll(SearchCriteria searchRequest);
}
