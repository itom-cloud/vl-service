package com.heycar.services.vl.throwable;

public class CSVDataFormateExcpetion extends RuntimeException {

	private static final long serialVersionUID = -6231473179084095203L;

	public CSVDataFormateExcpetion() {
		super();
	}
	
	public CSVDataFormateExcpetion(String message) {
		super(message);
	}

	public CSVDataFormateExcpetion(Throwable e) {
		super(e);
	}

	public CSVDataFormateExcpetion(String message, Throwable e) {
		super(message, e);
	}

}
