package com.heycar.services.vl.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.heycar.services.vl.throwable.CSVDataFormateExcpetion;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(CSVDataFormateExcpetion.class)
	public ResponseEntity<?> csvDataFormateError(CSVDataFormateExcpetion e) {
		log.error("Error:{}", e);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid CSV data or formate error");
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> internalServerError(Exception e) {
		log.error("Error: {}", e);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}
}
