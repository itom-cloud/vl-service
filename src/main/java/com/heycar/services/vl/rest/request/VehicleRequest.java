package com.heycar.services.vl.rest.request;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehicleRequest {
	@NotEmpty(message = "Code is a required field")
	private String code;
	@NotEmpty(message = "Make is a required field")
	private String make;
	@NotEmpty(message = "Model is a required field")
	private String model;
	@JsonAlias({ "kW", "power-in-ps" })
	@NotEmpty(message = "kW/Power-in-ps is a required field")
	private Integer power;
	@NotEmpty(message = "year is a required field")
	private Integer year;
	@NotEmpty(message = "color is a required field")
	private String color;
	@NotEmpty(message = "Price is a required field")
	private Integer price;
}
