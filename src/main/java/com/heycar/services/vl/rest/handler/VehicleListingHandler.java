package com.heycar.services.vl.rest.handler;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.heycar.services.vl.model.Vehicle;
import com.heycar.services.vl.rest.request.SearchCriteria;
import com.heycar.services.vl.rest.request.VehicleRequest;
import com.heycar.services.vl.rest.response.PageResponse;
import com.heycar.services.vl.rest.response.VehicleResponse;
import com.heycar.services.vl.service.VehicleService;
import com.heycar.services.vl.throwable.CSVDataFormateExcpetion;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class VehicleListingHandler {

	private final VehicleService service;

	public VehicleListingHandler(VehicleService service) {
		super();
		this.service = service;
	}

	public ResponseEntity<?> save(String dealerId, String csvdata) {
		service.saveAll(dealerId, convertCsv2VehicleRequest(csvdata));
		return ResponseEntity.ok().build();
	}

	public ResponseEntity<?> save(String dealerId, List<VehicleRequest> vehicleListings) {
		service.saveAll(dealerId, vehicleListings);
		return ResponseEntity.ok().build();
	}

	public List<VehicleRequest> convertCsv2VehicleRequest(String csv) {
		try {
			csv = csv.replaceAll("/", ",");
			CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
			CsvMapper mapper = new CsvMapper();
			MappingIterator<VehicleRequest> readValues = mapper.readerFor(VehicleRequest.class).with(bootstrapSchema)
					.readValues(csv);
			List<VehicleRequest> list = readValues.readAll();
			if (list.size() == 0)
				throw new CSVDataFormateExcpetion("Empty CSV Data");

			return list;
		} catch (Exception e) {
			log.error("Error: {}", e);
			throw new CSVDataFormateExcpetion(e);
		}
	}

	public ResponseEntity<PageResponse<VehicleResponse>> search(SearchCriteria searchRequest) {
		final Page<Vehicle> page = service.findAll(searchRequest);
		final Collection<VehicleResponse> content = page.stream().map(element -> VehicleResponse.from(element))
				.collect(Collectors.toList());
		final PageResponse<VehicleResponse> response = new PageResponse<VehicleResponse>(page.getSize(),
				page.getNumber(), page.getTotalElements(), content);
		return ResponseEntity.ok(response);
	}

}
