package com.heycar.services.vl.rest.response;

import java.time.LocalDateTime;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.heycar.services.vl.model.Vehicle;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class VehicleResponse {
	private String make;
	private String model;
	private Integer power;
	private Integer year;
	private String color;
	private Integer price;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updatedOn;

	public static VehicleResponse from(Vehicle vehicle) {
		VehicleResponse response = new VehicleResponse();
		BeanUtils.copyProperties(vehicle, response);
		return response;
	}
}
