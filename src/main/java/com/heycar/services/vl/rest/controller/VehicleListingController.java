package com.heycar.services.vl.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.heycar.services.vl.rest.handler.VehicleListingHandler;
import com.heycar.services.vl.rest.request.SearchCriteria;
import com.heycar.services.vl.rest.request.VehicleRequest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping(value = "/vehicle_listings/v1")
@Tag(name = "vehicle_listings")
@Validated
public class VehicleListingController {

	private final VehicleListingHandler handler;

	public VehicleListingController(final VehicleListingHandler writeHandler) {
		super();
		this.handler = writeHandler;
	}

	@PostMapping(value = "/upload_csv/{dealer_id}", consumes = MediaType.TEXT_PLAIN_VALUE)
	@Operation(summary = "Upload CSV Vehicle Data", description = "Upload CSV Vehicle Data", tags = "upload_csv")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Vehicle data has been saved successfully") })
	public ResponseEntity<?> uploadCSV(@PathVariable(value = "dealer_id") String dealerId, @RequestBody String csv) {
		return handler.save(dealerId, csv);
	}

	@PostMapping(value = "/upload_json/{dealer_id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "JSON Vehicle Data", description = "Submit JSON Vehicle Data", tags = "upload_json")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Vehicle data has been saved successfully") })
	public ResponseEntity<?> uploadJSON(@PathVariable(value = "dealer_id") String dealerId,
			@RequestBody @Valid List<@Valid VehicleRequest> vehicleListings) {
		return handler.save(dealerId, vehicleListings);
	}

	@GetMapping(value = "/search")
	public ResponseEntity<?> search(SearchCriteria searchRequest) {
		return handler.search(searchRequest);
	}

}
