package com.heycar.services.vl.rest.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SearchCriteria {

	private Integer pageNumber;
	private Integer pageSize;
	private String make;
	private String model;
	private Integer year;
	private String color;

}
