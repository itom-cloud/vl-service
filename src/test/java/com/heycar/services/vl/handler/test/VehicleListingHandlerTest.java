package com.heycar.services.vl.handler.test;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.compress.utils.IOUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.heycar.services.vl.model.Vehicle;
import com.heycar.services.vl.rest.handler.VehicleListingHandler;
import com.heycar.services.vl.rest.request.SearchCriteria;
import com.heycar.services.vl.rest.request.VehicleRequest;
import com.heycar.services.vl.service.VehicleService;
import com.heycar.services.vl.throwable.CSVDataFormateExcpetion;

import lombok.SneakyThrows;

@SpringBootTest
public class VehicleListingHandlerTest {

	final String dealerId = "1";

	@Mock
	VehicleService service;
	
	@InjectMocks
	VehicleListingHandler handler;

	@SneakyThrows
	@Test
	public void convertCsv2VehicleRequestPositiveTest() {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		IOUtils.copy(getClass().getResourceAsStream("/vehicle_data.csv"), bos);
		List<VehicleRequest> vehicles = handler.convertCsv2VehicleRequest(new String(bos.toByteArray()));
		Assert.assertTrue(vehicles.size() == 4);
	}

	@Test
	public void convertCsv2VehicleRequestNegativeTest() {
		Assert.assertThrows(CSVDataFormateExcpetion.class, () -> {
			handler.convertCsv2VehicleRequest("CSV");
		});
	}

	@SneakyThrows
	@Test
	public void uploadCSVTest() {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		IOUtils.copy(getClass().getResourceAsStream("/vehicle_data.csv"), bos);
		ResponseEntity<?> response = handler.save(dealerId, new String(bos.toByteArray()));
		Assert.assertTrue(response.getStatusCode() == HttpStatus.OK);
	}

	@SneakyThrows
	@Test
	public void uploadJSONTest() {
		ResponseEntity<?> response = handler.save(dealerId, getVehicleRequest());
		Assert.assertTrue(response.getStatusCode() == HttpStatus.OK);
	}

	@SneakyThrows
	@Test
	public void searchTest() {
		Mockito.when(service.findAll(Mockito.any(SearchCriteria.class))).thenReturn(getVehicleResponse());
		SearchCriteria searchRequest = new SearchCriteria();
		ResponseEntity<?> response = handler.search(searchRequest);
		Assert.assertTrue(response.getStatusCode() == HttpStatus.OK);
		Assert.assertTrue(response.hasBody());
	}

	@SneakyThrows
	private List<VehicleRequest> getVehicleRequest() {
		ObjectMapper mapper = new ObjectMapper();
		VehicleRequest[] array = null;
		try (InputStream in = getClass().getResourceAsStream("/vehicle_request_data.json")) {
			array = mapper.readValue(in, VehicleRequest[].class);
		}
		return Arrays.asList(array);
	}

	@SneakyThrows
	private Page<Vehicle> getVehicleResponse() {
		ObjectMapper mapper = new ObjectMapper();
		Vehicle[] array = null;
		try (InputStream in = getClass().getResourceAsStream("/vehicle_data.json")) {
			array = mapper.readValue(in, Vehicle[].class);
		}
		return new PageImpl<Vehicle>(Arrays.asList(array));
	}
}
