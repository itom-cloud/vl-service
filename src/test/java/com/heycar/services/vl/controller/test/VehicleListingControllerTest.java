package com.heycar.services.vl.controller.test;

import java.io.ByteArrayOutputStream;

import org.apache.commons.compress.utils.IOUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.heycar.services.vl.rest.controller.VehicleListingController;
import com.heycar.services.vl.rest.handler.VehicleListingHandler;

import lombok.SneakyThrows;

@SpringBootTest
public class VehicleListingControllerTest {

	MockMvc mockMvc;
	VehicleListingController controller;
	@Mock
	VehicleListingHandler handler;

	@Test
	@SneakyThrows
	public void uploadCSV() {
		controller = new VehicleListingController(handler);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		IOUtils.copy(getClass().getResourceAsStream("/vehicle_data.csv"), bos);
		mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8860/vehicle_listings/v1/upload_csv/1")
				.contentType(MediaType.TEXT_PLAIN_VALUE).content(bos.toByteArray()))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

	}
	
	@Test
	@SneakyThrows
	public void uploadJson() {
		controller = new VehicleListingController(handler);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		IOUtils.copy(getClass().getResourceAsStream("/vehicle_request_data.json"), bos);
		mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8860/vehicle_listings/v1/upload_json/1")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(bos.toByteArray()))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

	}

}
