package com.heycar.services.vl.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import com.heycar.services.vl.model.Vehicle;
import com.heycar.services.vl.rest.request.SearchCriteria;
import com.heycar.services.vl.rest.request.VehicleRequest;
import com.heycar.services.vl.service.impl.VehicleServiceImpl;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class VehicleServiceTest {

	@Autowired
	VehicleServiceImpl service;
	String dealerId = "1";

	@Test
	@Order(1)
	public void saveAllNewEntitiesTest() {
		List<VehicleRequest> vehicles = new ArrayList<VehicleRequest>();
		vehicles.add(new VehicleRequest("1", "BMW", "X1", 2400, 2020, "Blue", 180_000));
		vehicles.add(new VehicleRequest("2", "BMW", "X5", 2400, 2020, "Red", 220_000));
		service.saveAll(dealerId, vehicles);
	}

	@Test
	@Order(2)
	public void saveAllUpdateEntitiesTest() {
		List<VehicleRequest> vehicles = new ArrayList<VehicleRequest>();
		vehicles.add(new VehicleRequest("1", "BMW", "X1", 2400, 2020, "Blue", 180_000));
		vehicles.add(new VehicleRequest("2", "BMW", "X5", 2400, 2020, "Green", 220_000));
		vehicles.add(new VehicleRequest("3", "BMW", "X6", 2400, 2020, "Black", 220_000));
		service.saveAll(dealerId, vehicles);

		SearchCriteria searchRequest = SearchCriteria.builder().pageNumber(0).pageSize(10).color("Green").make("BMW")
				.build();
		Page<Vehicle> page = service.findAll(searchRequest);
		assertEquals(1, page.getTotalElements());
	}

	@Test
	@Order(3)
	public void findEntityWithSingleAttributeCriteriaPositiveTest() {
		SearchCriteria searchRequest = SearchCriteria.builder().pageNumber(0).pageSize(10).build();
		searchRequest.setColor("black");
		Page<Vehicle> page = service.findAll(searchRequest);
		assertTrue(page.getTotalElements() > 0);

		searchRequest = SearchCriteria.builder().pageNumber(0).pageSize(10).build();
		searchRequest.setMake("BMW");
		page = service.findAll(searchRequest);
		assertTrue(page.getTotalElements() > 0);

		searchRequest = SearchCriteria.builder().pageNumber(0).pageSize(10).build();
		searchRequest.setModel("X6");
		page = service.findAll(searchRequest);
		assertTrue(page.getTotalElements() > 0);

		searchRequest = SearchCriteria.builder().pageNumber(0).pageSize(10).build();
		searchRequest.setYear(2020);
		page = service.findAll(searchRequest);
		assertTrue(page.getTotalElements() > 0);
	}

	@Test
	@Order(4)
	public void findEntityWithMultiAttributeCriteriaPositiveTest() {
		SearchCriteria searchRequest = SearchCriteria.builder().pageNumber(0).pageSize(10).build();
		searchRequest.setColor("Blue");
		Page<Vehicle> page = service.findAll(searchRequest);
		assertTrue(page.getTotalElements() > 0);

		searchRequest.setMake("BMW");
		page = service.findAll(searchRequest);
		assertTrue(page.getTotalElements() > 0);

		searchRequest.setModel("X1");
		page = service.findAll(searchRequest);
		assertTrue(page.getTotalElements() > 0);

		searchRequest.setYear(2020);
		page = service.findAll(searchRequest);
		assertTrue(page.getTotalElements() > 0);
	}

	@Test
	@Order(5)
	public void findEntityNegativeTest() {
		SearchCriteria searchRequest = SearchCriteria.builder().pageNumber(0).pageSize(10).color("yellow").build();
		Page<Vehicle> page = service.findAll(searchRequest);
		assertTrue(page.getTotalElements() == 0);
	}
}
