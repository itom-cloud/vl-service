package com.heycar.services.vl.test;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.commons.compress.utils.IOUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.heycar.services.vl.Application;
import com.heycar.services.vl.rest.handler.VehicleListingHandler;
import com.heycar.services.vl.rest.request.SearchCriteria;
import com.heycar.services.vl.rest.request.VehicleRequest;

import lombok.SneakyThrows;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("dev")
@TestPropertySource(properties = "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration")
@TestMethodOrder(OrderAnnotation.class)
public class VehicleListingIT {

	@Autowired
	VehicleListingHandler handler;

	@Test
	@Order(1)
	@SneakyThrows
	public void uploadCSV() {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		IOUtils.copy(getClass().getResourceAsStream("/vehicle_data.csv"), bos);
		handler.save("1", new String(bos.toByteArray()));
	}

	@Test
	@Order(2)
	@SneakyThrows
	public void uploadJson() {
		ObjectMapper mapper = new ObjectMapper();
		VehicleRequest[] array = null;
		try (InputStream in = getClass().getResourceAsStream("/vehicle_request_data.json")) {
			array = mapper.readValue(in, VehicleRequest[].class);
		}
		handler.save("1", Arrays.asList(array));
	}

	@Test
	@Order(3)
	@SneakyThrows
	public void search() {
		SearchCriteria searchRequest = SearchCriteria.builder().pageNumber(0).pageSize(10).color("red").build();
		Assert.assertTrue(handler.search(searchRequest).getBody().getTotalElements() > 0);
	}

}
