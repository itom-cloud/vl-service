# README #

This README helps to compile, test and run the code 

Spring Profiles:
* default : For Local Mongodb Storage
* live : For Cloud Mongodb Storage

-Dspring.profiles.active=[dev | live]

## Project ##
Vehicle Listing Service

### What is this repository for? ###
* Coding challenge

### How do I get set up? ###

* Install Java 8
* Install Maven (Latest)
* Lombok setup please visit [https://projectlombok.org/setup/overview]

### Build, Test and Run ###

* mvn package
* java -jar vl-service-1.0.0-SNAPSHOT.jar -Dspring.profiles.active=[dev | live]
* Test with Swagger on your browser Local [http://localhost:8860/swagger-ui/index.html?url=/v3/api-docs]
* Test with Swagger on your browser Live  [http://139.9.50.244:8860/swagger-ui/index.html?url=/v3/api-docs]

### Code Coverage ###
Coverage: 50%
Covered Instructions: 1,322
Missed Instructions: 1,322
Total Instructions: 2,644

### Who do I talk to? ###

* mysialkot@hotmail.com